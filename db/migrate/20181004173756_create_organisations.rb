class CreateOrganisations < ActiveRecord::Migration[5.1]
  def change
    
    create_table :organisations do |t|
      t.string :org_name
      t.string :org_identifier
      t.string :city
      t.string :state
      t.string :country
      t.string :zip_code

      t.timestamps
    end
  end
end
