class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.text :first_address
      t.text :second_address

      t.timestamps
    end
  end
end
