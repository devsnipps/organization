class Add < ActiveRecord::Migration[5.1]
  def change
  	add_column :profiles, :organisation_id, :integer
  	add_column :addresses, :organisation_id, :integer
    add_index :profiles, :organisation_id
    add_index :addresses, :organisation_id
  end
end
