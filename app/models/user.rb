class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one :organisation
  

  #after_create :create_org
  #def create_org
  #	redirect_to new_after_register_path
  #end

  #validations
  validates :email, presence: true, uniqueness: true
  validates :password, length: { in: 6..20 }
  validates_confirmation_of :password, if: :password_required?
  
end
