class Organisation < ApplicationRecord
	belongs_to :user, required: false

	has_one :profile
	has_one :address

	accepts_nested_attributes_for :profile
	accepts_nested_attributes_for :address

	#callbacks

	#before_create :add_user

	#def add_user
	#	self.user_id = current_user
	#end

	#validations

	validates :org_name, presence: true, uniqueness: true
	validates :city, :state, :country, presence: true
	validates :zip_code, numericality: true, length: { is: 6 }
end
