class OrganisationsController < ApplicationController
	before_action :authenticate_user!
	before_action :check_user_orga
	#include Wicked::Wizard

    #steps :organization

    def new
    	
    	@organisation = Organisation.new
		@organisation.build_profile
    	@organisation.build_address    	

    end

    def create
    	@organisation = Organisation.new(organisation_params)
    	@organisation.user = current_user
    	

    	if @organisation.save
    		 redirect_to home_index_path
    	else
    		render :new
    	end

    	
    end

    private
		def organisation_params
		    params.require(:organisation).permit(:org_name, :org_identifier,:user_id, :zip_code, :city, :state, :country, profile_attributes: [:first_name, :last_name], address_attributes: [:first_address, :second_address])
		end

		def check_user_orga
			unless !current_user.organisation
				redirect_to home_index_path
			end
		end



end
