class AdminController < ApplicationController
  before_action :authenticate_user!
  before_action :check_admin

  def index
  	#if  !params[:q]
  	#	@organization = Organisation.all.joins(:profile, :address)
  	#else
  		
  		@search = Organisation.joins(:profile, :address).search(params[:q])#.joins(:profile, :address)
  		@organizations = @search.result.paginate(:page => params[:page], :per_page => 10)
  	#end
  end

  private

  	def check_admin
  		unless current_user.admin?
  			redirect_to home_index_path
  			
  		end
  	end
end
