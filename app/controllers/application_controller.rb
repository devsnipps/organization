class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception


  	def after_sign_in_path_for(resource)
  		if current_user.organisation
  			flash[:notice] = 'You Already created your Organization.' 
	  		home_index_path
	  	else
	  		 new_organisation_path
	  	end
	end

  	#before_action :users_check
  	#def users_check
		#@user = current_user
	#	if user_signed_in?
	#		if current_user == Organisation.find_by(params[:user_id])
	#			redirect_to admin_index_path and return true
	#		else
	#			render home_index_path and return true
	#		end
	#	end
	#end

  
end
