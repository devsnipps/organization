Rails.application.routes.draw do
  get '/admins', to: 'admin#index'

  get 'home/index'

	devise_for :users, controllers: {
        registrations: 'registrations'
      }
	root :to => redirect("/users/sign_up")
  
  resources :organisations
	
  #devise_scope :users do
	#  root "users/sign_in", to: "devise/sessions#new"
	#end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
