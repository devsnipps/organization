require 'rails_helper'

RSpec.describe Address, type: :model do
  it { should validate_presence_of(:first_address) }
  
  it { should belong_to(:organisation) }
end
