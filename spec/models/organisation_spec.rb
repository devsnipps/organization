require 'rails_helper'

RSpec.describe Organisation, type: :model do
  
  describe 'attributes' do
    it 'has a org_name' do
      organisation = build(:organisation)
      expect(organisation).to respond_to(:org_name)
    end
  end
  
  describe 'validations' do
    it { should validate_uniqueness_of(:org_name)}
    it { should validate_presence_of(:city)}
    it { should validate_presence_of(:state)}
    it { should validate_presence_of(:country)}
    it { should validate_numericality_of(:zip_code)}
    it { should validate_length_of(:zip_code).is_equal_to(6)}
  end
  
  describe 'accepts nested atributes' do
  
    it { should accept_nested_attributes_for(:address)}
    it { should accept_nested_attributes_for(:profile)}
  end
  
  describe 'relationships' do
    it { should have_one(:address)}
    it { should have_one(:profile)}
  end
  
end
